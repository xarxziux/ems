package main

import (
	"ems/internal/transport/graphql"
)

func main() {
	server := graphql.NewServer()
	_ = server.StartServer()
}
