test:
	ginkgo -r
lint:
	find internal -type f -name *.go -exec gofmt -s -w {} \;
	find internal -type f -name *.go -exec goimports -w {} \;
	golangci-lint run -c .golangci.yml
image:
	docker build -t ems:latest .
run:
	go run cmd/ems/ems.go
run_docker:
	docker run -p 8080:8080 ems
