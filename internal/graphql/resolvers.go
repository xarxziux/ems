package graphql

import (
	"ems/internal/data"
	"ems/internal/models"
)

type EmployeeResolver struct {
	Emp *models.Employee
}

func (e *EmployeeResolver) ID() string {
	return e.Emp.ID
}

func (e *EmployeeResolver) FirstName() *string {
	return &e.Emp.FirstName
}

func (e *EmployeeResolver) LastName() *string {
	return &e.Emp.LastName
}

func (e *EmployeeResolver) Username() *string {
	return &e.Emp.Username
}

func (e *EmployeeResolver) Password() *string {
	return &e.Emp.Password
}

func (e *EmployeeResolver) Email() *string {
	return &e.Emp.Email
}

func (e *EmployeeResolver) DOB() *string {
	return &e.Emp.DOB
}

func (e *EmployeeResolver) DeptID() *string {
	return &e.Emp.DeptID
}

func (e *EmployeeResolver) Position() *string {
	return &e.Emp.Position
}

type Resolver struct{}

func (r *Resolver) GetAllEmployees() []*models.EmployeeResolver {
	return data.GetAllEmployees()
}

func (r *Resolver) GetEmployee(id models.GID) (*models.EmployeeResolver, error) {
	return data.GetEmployeeByID(id.ID)
}

func (r *Resolver) AddEmployee(emp models.Employee) (*models.EmployeeResolver, error) {
	return data.AddEmployee(emp)
}
func (r *Resolver) UpdateEmployee(emp models.Employee) (*models.EmployeeResolver, error) {
	return data.UpdateEmployee(emp)
}

func (r *Resolver) DeleteEmployee(gid models.GID) (*models.EmployeeResolver, error) {
	return data.DeleteEmployee(gid.ID)
}
