package graphql

import (
	"io/ioutil"

	graphql "github.com/graph-gophers/graphql-go"
)

func GetHandler() (*Handler, error) {
	rawSchema, err := ioutil.ReadFile("./internal/graphql/schema.gql")
	if err != nil {
		return nil, err
	}

	schema := graphql.MustParseSchema(string(rawSchema), &Resolver{})
	handler := Handler{Schema: schema}

	return &handler, nil
}
