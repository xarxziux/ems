package data

import (
	"errors"
	"fmt"

	"ems/internal/models"
)

var employeeList map[models.Employee]struct{}

func init() {
	employeeList = map[models.Employee]struct{}{}

	employeeList[models.Employee{
		ID:        "1",
		FirstName: "Alice",
		LastName:  "Smith",
		Username:  "asmith",
		Password:  "secret",
		Email:     "asmith@dog.com",
		DOB:       "13-05-95",
		DeptID:    "323",
		Position:  "missionary",
	}] = struct{}{}

	employeeList[models.Employee{
		ID:        "2",
		FirstName: "Bob",
		LastName:  "Jones",
		Username:  "bjones",
		Password:  "password",
		Email:     "bjones@dog.com",
		DOB:       "09-01-87",
		DeptID:    "611",
		Position:  "doggy",
	}] = struct{}{}

	employeeList[models.Employee{
		ID:        "3",
		FirstName: "Catherine",
		LastName:  "Dobbs",
		Username:  "cdobbs",
		Password:  "correcthorsebatterystaple",
		Email:     "cdobbs@dog.com",
		DOB:       "28-11-01",
		DeptID:    "911",
		Position:  "cowgirl",
	}] = struct{}{}
}

func GetAllEmployees() []*models.EmployeeResolver {
	out := make([]*models.EmployeeResolver, 0, len(employeeList))

	for emp := range employeeList {
		emp := emp
		out = append(out, &models.EmployeeResolver{Emp: &emp})
	}

	return out
}

func GetEmployeeByID(id string) (*models.EmployeeResolver, error) {
	for emp := range employeeList {
		if id == emp.ID {
			emp := emp

			return &models.EmployeeResolver{
				Emp: &emp,
			}, nil
		}
	}

	return nil, fmt.Errorf("no record found for id %s", id)
}

func AddEmployee(emp models.Employee) (*models.EmployeeResolver, error) {
	return nil, errors.New("Unimplemented")
}
func UpdateEmployee(emp models.Employee) (*models.EmployeeResolver, error) {
	return nil, errors.New("Unimplemented")
}

func DeleteEmployee(id string) (*models.EmployeeResolver, error) {
	return nil, errors.New("Unimplemented")
}
