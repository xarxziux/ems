package models

type Employee struct {
	ID        string
	FirstName string
	LastName  string
	Username  string
	Password  string
	Email     string
	DOB       string
	DeptID    string
	Position  string
}

type EmployeeResolver struct {
	Emp *Employee
}

func (e *EmployeeResolver) ID() string {
	return e.Emp.ID
}

func (e *EmployeeResolver) FirstName() *string {
	return &e.Emp.FirstName
}

func (e *EmployeeResolver) LastName() *string {
	return &e.Emp.LastName
}

func (e *EmployeeResolver) Username() *string {
	return &e.Emp.Username
}

func (e *EmployeeResolver) Password() *string {
	return &e.Emp.Password
}

func (e *EmployeeResolver) Email() *string {
	return &e.Emp.Email
}

func (e *EmployeeResolver) DOB() *string {
	return &e.Emp.DOB
}

func (e *EmployeeResolver) DeptID() *string {
	return &e.Emp.DeptID
}

func (e *EmployeeResolver) Position() *string {
	return &e.Emp.Position
}

type GID struct {
	ID string
}
