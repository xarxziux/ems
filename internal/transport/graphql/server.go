package graphql

import (
	"ems/internal/graphql"
	"fmt"

	"github.com/gofiber/fiber/v2"
)

type Server struct{}

func NewServer() *Server {
	server := Server{}
	return &server
}

func (*Server) StartServer() error {
	app := fiber.New()

	handler, err := graphql.GetHandler()
	if err != nil {
		fmt.Printf("error reading schema file %v", err.Error())
		return err
	}

	app.Post("/graphql", handler.ServeHTTP)

	err = app.Listen(":8080")
	if err != nil {
		return err
	}

	return nil
}
