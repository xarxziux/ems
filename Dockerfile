FROM golang:1.19

WORKDIR /usr/src/app

# pre-copy/cache go.mod for pre-downloading dependencies and only redownloading them in subsequent builds if they change
COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY . .
RUN mkdir /usr/local/bin/ems/
RUN go build -v -o /usr/local/bin/ems ./...
EXPOSE 8080

CMD ["/usr/local/bin/ems/ems"]